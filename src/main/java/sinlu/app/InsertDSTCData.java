package sinlu.app;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import sinlu.model.Constants;
import sinlu.model.db.Domain;

public class InsertDSTCData {
	
	private Domain db;
	private String filePath = "DSTC/test1";
	private String domain = "route";
//	private FileWriter writer; 
	
	public static void main(String[] args) {
		try {
			new InsertDSTCData().run();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void run() throws SQLException, IOException {
		
//		String testFile = Constants.BASE_FILE_PATH + domain + "/test";
//		writer = new FileWriter(testFile);
		
		db = new Domain();
		
		db.createNewDomain(domain);
		db.setDomain(domain);
		
		try { 
			walk(filePath);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			destroy();
		}

//		writer.close();
		
	}

	private void destroy() {
		new File(Constants.BASE_FILE_PATH + domain + "/" + domain + ".db").delete();
		new File(Constants.BASE_FILE_PATH + domain).delete();		
	}

	private void walk(String filePath2) throws Exception {
		for (File file : new File(filePath2).listFiles()) {
			if (file.isDirectory()) walk(file.getAbsolutePath());
			else if (file.getName().endsWith("labels.json")) 
				handleJSONFile(file.getAbsolutePath());
		}
	}

	private void handleJSONFile(String absolutePath) throws Exception {
			
			JSONTokener tok = new JSONTokener(new URI("file://"+absolutePath).toURL().openStream());
			JSONObject json = new JSONObject(tok);
			
			for(Object turn : json.getJSONArray("turns")) {
				JSONObject turnJson = new JSONObject(turn.toString());
				String transc = turnJson.getString("transcription").trim();
				if ("NON_UNDERSTANDABLE".equals(transc)) continue;
				
				for (Object slulab : turnJson.getJSONArray("slu-labels")) {
					JSONObject slulabJson = new JSONObject(slulab.toString());
					
					JSONObject slots = slulabJson.getJSONObject("slots");
					
					if (slots.has("route") && slulabJson.getBoolean("label")){
						String route = slots.getString("route");
						System.out.println(route + "\t" + transc);
//						writer.write(route + "\t" + transc + "\n");
						List<String> concepts = db.getConcepts();
						if (!concepts.contains(route)) db.addNewConcept(route);
						db.addExampleForConcept(route, transc.replaceAll("[^a-zA-Z ]", "").toLowerCase());
					}
				}
			}
			
	}
			
			
		
	

}
