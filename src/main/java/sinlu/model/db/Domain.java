package sinlu.model.db;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sinlu.model.Constants;
import sium.nlu.language.LingEvidence;

/*
 * Interface to the sqlite database. There is an individual sqlite database for each domain. 
 */

public class Domain {

	protected Connection conn;
	protected Statement stat;
	
	public List<String> getDomains() {
		
		ArrayList<String> domains = new ArrayList<String>();
		for (File f : new File(Constants.BASE_FILE_PATH).listFiles()) {
			if (f.isDirectory()) domains.add(f.getName());
		}
		return domains;
	}
	
	public List<String> getConcepts() throws SQLException {
		Statement stat = createStatement();
		ResultSet result = stat.executeQuery(String.format("SELECT distinct concept FROM concept"));
		List<String> concepts = new ArrayList<String>();
		while (result.next()) {
			concepts.add(result.getString("concept"));
		}
		
		return concepts;
	}
	
	public List<Integer> getConceptIDs() throws SQLException {
		Statement stat = createStatement();
		ResultSet result = stat.executeQuery(String.format("SELECT distinct cid FROM concept"));
		List<Integer> concepts = new ArrayList<Integer>();
		while (result.next()) {
			concepts.add(result.getInt("cid"));
		}
		return concepts;
	}
	
	public String getConcept(Integer cid) throws SQLException {
		Statement stat = createStatement();
		ResultSet result = stat.executeQuery(String.format("SELECT concept FROM concept WHERE cid=%d", cid));
		return result.getString("concept");
	}
	
	public void addNewConcept(String concept) throws SQLException {
		Statement stat = createStatement();
		stat.execute(String.format("INSERT INTO concept (concept) VALUES ('%s')", concept));
	}
	
	public void createNewDomain(String s) throws SQLException {
		File dirs = new File(Constants.BASE_FILE_PATH + s);
		dirs.mkdirs();
		createDB(s);
	}

	private void createDB(String s) throws SQLException {
		setDomain(s);
		createEmptyTables(s);
		
	}

	private void createEmptyTables(String s) throws SQLException {
		Statement stat = createStatement();
		stat.execute(String.format("CREATE TABLE concept (cid INTEGER PRIMARY KEY AUTOINCREMENT, concept TEXT)"));
		stat.execute(String.format("CREATE TABLE word (wid INTEGER PRIMARY KEY AUTOINCREMENT, word TEXT)"));
		stat.execute(String.format("CREATE TABLE sequence (left INTEGER, right INTEGER, cid INTEGER)"));
	}

	public int getNumberOfConcepts() throws SQLException {
		Statement stat = createStatement();
		ResultSet result = stat.executeQuery(String.format("SELECT count(*) c FROM concept"));
		return result.getInt("c");
	}

	public void setDomain(String d) throws SQLException {
		createConnection(Constants.BASE_FILE_PATH + d + "/" + d + ".db");
	}
	
	public void createConnection(String path) throws SQLException {
		try {
			Class.forName("org.sqlite.JDBC");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		conn =  DriverManager.getConnection("jdbc:sqlite:" + path);
	}
	
	public Statement createStatement() throws SQLException {
		if (stat == null)
			stat = conn.createStatement();
		return stat;
	}
	
	public void closeConnection() throws SQLException {
		conn.close();
	}
	
	private int getConceptID(String concept) throws SQLException {
		Statement stat = createStatement();
		ResultSet result = stat.executeQuery(String.format("SELECT cid FROM concept WHERE concept='%s'", concept));
		return result.getInt("cid");
	}

	public void addExampleForConcept(String concept, String example) throws SQLException {
		int cid = getConceptID(concept);
		example = Constants.S_TAG + " " + example + " " + Constants.S_TAG;
		ArrayList<String> splitExample = new ArrayList<String>(Arrays.asList(example.split("\\s+")));
		for (int i=0; i<splitExample.size()-1; i++) {
			addWordPair(splitExample.get(i), splitExample.get(i+1), cid);
		}
		
	}

	public void addWordPair(String word1, String word2, int cid) throws SQLException {
		int w1 = offerWord(word1);
		int w2 = offerWord(word2);
		addSequence(w1, w2, cid);
	}

	public int offerWord(String word) throws SQLException {
		Statement stat = createStatement();
		ResultSet result = stat.executeQuery(String.format("SELECT wid FROM word WHERE word='%s'", word));
		if (result.isAfterLast()) {
			stat.execute(String.format("INSERT INTO word (word) VALUES ('%s')", word));
			return getMaxWordID();
		}
		else {
			return result.getInt("wid");	
		}
		
	}
	
	private int getMaxWordID() throws SQLException {
		Statement stat = createStatement();
		ResultSet result = stat.executeQuery(String.format("SELECT max(wid) w FROM word"));
		return result.getInt("w");
	}	

	private void addSequence(int w1, int w2, int cid) throws SQLException {
		Statement stat = createStatement();
		stat.execute(String.format("INSERT INTO sequence (left, right, cid) VALUES (%d, %d, %d)", w1, w2, cid));
	}
	
	public String getWordFromID(int id) throws SQLException {
		Statement stat = createStatement();
		ResultSet result = stat.executeQuery(String.format("SELECT word FROM word WHERE wid=%d", id));
		return result.getString("word");		
	}

	public List<LingEvidence> getUtterancesForConcept(int cid) throws SQLException {
		
		ArrayList<LingEvidence> ling = new ArrayList<LingEvidence>();
		Statement stat = createStatement();
		String query = "SELECT w1.word as word1, w2.word as word2 FROM sequence, word w1, word w2 WHERE w1.wid = sequence.left AND w2.wid = sequence.right AND cid = %d";
		ResultSet result = stat.executeQuery(String.format(query, cid));
		
		while (result.next()) {
			LingEvidence l = new LingEvidence();
			l.addEvidence("w1", result.getString("word1"));
			l.addEvidence("w2", result.getString("word2"));
			ling.add(l);
		}
		
		return ling;
		
	}







}
