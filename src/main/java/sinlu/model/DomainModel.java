package sinlu.model;

import java.sql.SQLException;

import sinlu.model.db.Domain;
import sium.nlu.context.Context;
import sium.nlu.grounding.Grounder;
import sium.nlu.language.LingEvidence;
import sium.nlu.language.mapping.MaxEntMapping;
import sium.nlu.stat.Distribution;

/*
 * This class is an interface between an user interface and SINLU. The idea is to be able to do some simple NLU
 * with a minimal amount of training data, but still scalable to using larger amounts of data. 
 */

public class DomainModel {
	
	private MaxEntMapping mapping;
	private Domain db;
	private String domain;
	private LingEvidence ling;
	private Grounder<String,String> grounder;
	private boolean trainingComplete;
	
	public DomainModel(Domain db, String domain) {
		setTrainingComplete(false);
		grounder = new Grounder<String,String>(); // the grounder is part of SIUM
		ling = new LingEvidence(); // also part of SIUM
		mapping = new MaxEntMapping(Constants.BASE_FILE_PATH + domain + "/" + domain + ".txt"); // the thing that maps between language (in this case, ngrams) and high-level concepts
		this.setDB(db);
		this.setDomain(domain);
	}
	
	/*
	 * This takes information out of the database and trains a maxent model. The class labels are the concepts. The features
	 * are a bunch of bigrams. Essentially, we are training concept-level language models. 
	 */
	public void train() throws SQLException {
		for (Integer cid : db.getConceptIDs()) {
			Context<String,String> context = new Context<String,String>();
			String concept = db.getConcept(cid);
			context.addPropertyToEntity(concept, concept);
			for (LingEvidence ling : db.getUtterancesForConcept(cid)) {
				mapping.addEvidenceToTrain(ling, context.getPropertiesForEntity(concept));
			}
		}
		mapping.train();
		setTrainingComplete(true);
	}
	
	public void newUtterance() {
		grounder.clear();
		ling = new LingEvidence();
	}
	
	/*
	 * The Context object is from SIUM. It holds information about what can be predicted. In this case, 
	 * we want a distribution over all the concepts (their properties are themselves...something brought
	 * over from the reference resolution literature...I won't explain that any further). 
	 */
	public Context<String,String> getContext() {
		Context<String,String> context = new Context<String,String>();
		
		try {
			for (String concept : db.getConcepts()) {
				context.addPropertyToEntity(concept, concept);
			}
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return context;
	}
	
	/*
	 * Words are evaluated indivdually; this keeps track of bigrams for features. 
	 */
	public Distribution<String> addIncrement(String word) {
		
		if (ling.hasKey("w1"))
			ling.addEvidence("w2", ling.getValue("w1"));
		ling.addEvidence("w1", word);
		grounder.groundIncrement(getContext(), mapping.applyEvidenceToContext(ling));
		return grounder.getPosterior();
		
	}
	
	public void close() {
		mapping.clear();
	}
	

	public Domain getDB() {
		return db;
	}

	public void setDB(Domain db) {
		this.db = db;
	}
	
	public String getDomain() {
		return domain;
	}

	public void setDomain(String d) {
		this.domain = d;
	}

	public Distribution<String> getPosterior() {
		return grounder.getPosterior();
	}

	public boolean isTrainingComplete() {
		return trainingComplete;
	}

	public void setTrainingComplete(boolean trainingComplete) {
		this.trainingComplete = trainingComplete;
	}
	

}
