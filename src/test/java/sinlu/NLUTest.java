package sinlu;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import sium.nlu.context.Context;
import sium.nlu.grounding.Grounder;
import sium.nlu.language.LingEvidence;
import sium.nlu.language.mapping.MaxEntMapping;
import sium.nlu.language.mapping.NaiveBayesMapping;

public class NLUTest {

	@Test public void testSomeLibraryMethod() {
		
		//NaiveBayesMapping nbmap = new NaiveBayesMapping();
		MaxEntMapping nbmap =  new MaxEntMapping("temp.txt");
		
		Context<String,String> context = new Context<String,String>();
		context.addPropertyToEntity("location", "location");
		context.addPropertyToEntity("time/date", "time/date");
		context.addPropertyToEntity("category", "category");
		context.addPropertyToEntity("price", "price");
		
		LingEvidence evidence = new LingEvidence();
		evidence.addEvidence("w1", "ich");
		evidence.addEvidence("w1", "suche");
		evidence.addEvidence("w1", "was");
		evidence.addEvidence("w1", "in");
		evidence.addEvidence("w1", "schildesche");
		nbmap.addEvidenceToTrain(evidence, context.getPropertiesForEntity("location"));
		evidence.addEvidence("w1", "oder");
		evidence.addEvidence("w1", "heepen");
		nbmap.addEvidenceToTrain(evidence, context.getPropertiesForEntity("location"));
		evidence.addEvidence("w1", "heute");
		nbmap.addEvidenceToTrain(evidence, context.getPropertiesForEntity("time/date"));
		evidence.addEvidence("w1", "abend");
		nbmap.addEvidenceToTrain(evidence, context.getPropertiesForEntity("time/date"));
		
		nbmap.train();
		
		
		Grounder<String,String> grounder = new Grounder<String,String>();
		
		grounder.groundIncrement(context, nbmap.applyEvidenceToContext(evidence));
		
		System.out.println(grounder.getPosterior());
		
		
		
	}
	
}
