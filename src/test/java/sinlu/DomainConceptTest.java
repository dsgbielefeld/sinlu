package sinlu;


import static org.junit.Assert.assertTrue;

import java.io.File;
import java.sql.SQLException;

import org.junit.Test;

import sinlu.model.db.Domain;
import sium.nlu.stat.Distribution;
import sinlu.model.Constants;
import sinlu.model.DomainModel;

public class DomainConceptTest {
	
	DomainModel model;
	
	@Test public void test() {
		Domain db = new Domain();
		String d = "t_food";
		try {
			create(db, d);
			train(db, d);
			eval();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			destroy(d);
		}
	}

	private void eval() {
		model.addIncrement("a");
		model.addIncrement("reference");
		model.addIncrement("to");
		model.addIncrement("concept");
		Distribution<String> dist = model.addIncrement("one");
		assertTrue(dist.getProbabilityForItem("concept1") > dist.getProbabilityForItem("concept2"));
		model.newUtterance();
		model.addIncrement("a");
		model.addIncrement("reference");
		model.addIncrement("to");
		model.addIncrement("concept");
		dist = model.addIncrement("two");
		assertTrue(dist.getProbabilityForItem("concept1") < dist.getProbabilityForItem("concept2"));
	}

	private void train(Domain db, String d) throws SQLException {
		model = new DomainModel(db, d);
		model.train();
	}

	private void destroy(String d) {
		if (model != null)
			model.close();
		new File(Constants.BASE_FILE_PATH + d + "/" + d + ".db").delete();
		new File(Constants.BASE_FILE_PATH + d).delete();
	}

	private void create(Domain db, String d) throws SQLException {

		
		db.createNewDomain(d);
		db.setDomain(d);
		
		// does the database now exist?
		assertTrue(new File(Constants.BASE_FILE_PATH + d + "/" + d + ".db").exists());
		
		db.addNewConcept("concept1");
		db.addNewConcept("concept2");
		
		// have the two concepts been added, and do the corresponding queries work?
		assertTrue(db.getConcepts().size() == 2);
	
		assertTrue(db.offerWord("one") == db.offerWord("one"));
		assertTrue(db.offerWord("one") < db.offerWord("two"));
		
		db.addExampleForConcept("concept1", "example for concept one");
		db.addExampleForConcept("concept1", "another way of referring to concept one");
		db.addExampleForConcept("concept2", "example for concept two");
		db.addExampleForConcept("concept2", "another way of referring to concept two");
		db.addExampleForConcept("concept2", "yet another way of referring to concept two");
		
		
		
		
	}
	

}
